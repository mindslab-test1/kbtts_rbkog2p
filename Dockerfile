FROM python:3.8.5
RUN python3 -m pip --no-cache-dir install --upgrade \
        kiwipiepy==0.9.3 \
        grpcio==1.38.0 \
        grpcio-tools==1.38.0 \
        grpcio-health-checking==1.38.0 \
        protobuf==3.15.8 \
        && \
mkdir /root/rbkog2p
COPY . /root/rbkog2p
RUN cd /root/rbkog2p && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. g2p.proto
EXPOSE 19002
WORKDIR /root/rbkog2p
ENTRYPOINT python server.py
