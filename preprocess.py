import os, csv, json
import requests

import hangeul

def sanitize(word, listed=False, number=False):
	if listed:
		s = "".join(x for x in word if hangeul.is_hangeul(x) or x == '[' or x == ']' or x == '/')
		return [(lambda x,y: (x, y.split('/')[0]))(*x.split('[')) for x in s.split(']') if len(x.split('[')) == 2]
	else:
		s = "".join(x for x in word.split('/')[0] if hangeul.is_hangeul(x))
		n = "".join(x for x in word.split('/')[0] if x in '0123456789')
		return (s, int(n) if n else 0) if number else s

form_dict = {
	'명사': 'NN', '의존명사': 'NNB', '대명사': 'NP', '수사': 'NR',
	'동사': 'VV', '보조동사': 'VX', '형용사': 'VA', '보조형용사': 'VX', '어미': 'E',
	'관형사': 'MM', '부사': 'MA',
	'조사': 'J', '접사': 'XZ',
	'감탄사': 'IC',
	'구': 'NA', '품사없음': "",
}
def form_change(x):
	res = []
	for y in x.split('」'):
		s = sanitize(y.strip())
		if s:
			res.append(form_dict[s])
	return res

def load_dict():
	dictionary = {}
	files = os.listdir('./dict')
	error = []
	for idx, fn in enumerate(files):
		with open('dict/' + fn, 'r', encoding='utf-8') as f:
			rdr = csv.reader(f, delimiter='\t')
			r = list(rdr)
		header = r[0]
		exp, form_idx, phoneme_idx, change_idx = [header.index(x) for x in ['어휘', '품사', '발음', '활용']]
		for i, x in enumerate(r[1:]):
			if i % 300 == 0:
				print('\r%6.2f%% (%2d / %2d)' % ((i + 1) / (len(r) - 1) * 100, idx + 1, len(files)), end="")
			key, num = sanitize(x[exp], number=True)
			form = form_change(x[form_idx])
			phoneme = sanitize(x[phoneme_idx])
			change = sanitize(x[change_idx], listed=True)
			for f in form:
				if (key + f) not in dictionary:
					dictionary[key + f] = {}
				dictionary[key + f][num] = phoneme
			for f in form:
				for k, v in change:
					if (k + 'Z' + f) not in dictionary:
						dictionary[k + 'Z' + f] = {}
					dictionary[k + 'Z' + f][num] = v
		print('\r100.00%% (%2d / %2d)' % (idx + 1, len(files)), end="")
	dictionary = {key: {num: phoneme for num, phoneme in val.items() if phoneme} for key, val in dictionary.items()}
	print()

	for key, val in dictionary.items():
		complete_phoneme = {phoneme for _, phoneme in val.items()}
		if len(complete_phoneme) >= 2:
			word = key
			for i in range(len(word) -1, -1, -1):
				if ord(word[i]) >= 0x80:
					word, form = word[:i + 1], word[i + 1:]
					break
			else:
				word = ''
				form = ''
			if form.startswith('Z'):
				form = ''
			if word and form:
				try:
					enc = "".join('%%%02X' % x for x in word.encode())
					req = requests.get('https://ko.dict.naver.com/api3/koko/search?query=%s&m=pc&range=word&page=1&shouldSearchOpen=false' % enc)
					data = json.loads(req.text)
					data = data['searchResultMap']['searchResultListMap']['WORD']['items']
					data = [(x['meansCollector'][0]['partOfSpeech'], x['expEntrySuperscript']) for x in data if x['handleEntry'] == word and x['sourceDictnameKO'] == '표준국어대사전']
					data = [int(y) for x,y in data if x in form_dict and form_dict[x] == form]
					res = []
					for idx in data:
						the_phoneme = val[idx]
						if the_phoneme in complete_phoneme:
							res.append(the_phoneme)
							complete_phoneme.remove(the_phoneme)
					if len(complete_phoneme) <= 1:
						res += list(complete_phoneme)
						dictionary[key] = res
						print('resolved automatically: %s%s' % (word, form), '/', res)
					else:
						raise ValueError(word)
				except:
					error.append(word)
					complete_phoneme = {phoneme for _, phoneme in val.items()}
					dictionary[key] = list(complete_phoneme)
			else:
				dictionary[key] = list(complete_phoneme) # TODO:
		else:
			dictionary[key] = list(complete_phoneme)

	return dictionary, error

def main():
	dictionary, errors = load_dict()
	l = []
	for i, (key, val) in enumerate(dictionary.items()):
		if i % 200 == 0:
			print('\r%6.2f%%' % ((i + 1) / len(dictionary) * 100), end="")
		l.append((key + ' ' + ' '.join(list(val))).strip() + '\n')
	l.sort(key=lambda x:x.split(' ')[0])
	print()
	with open('word_dict.txt', 'w') as f:
		f.write("".join(l))
	with open('error.txt', 'w') as f:
		f.write('\n'.join(errors))

if __name__ == '__main__':
	main()
