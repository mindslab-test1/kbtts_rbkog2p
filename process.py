from kiwipiepy import Kiwi, Option
import hangeul, algorithm

import os, csv, sys
import traceback

def find_dict(dict, x, y):
	try:
		if y[0] == 'V':
			xx = x[:-1]
			suff = '다'
		else:
			xx = x
			suff = ""
		if xx + 'Z' + y in dict:
			return dict[xx + 'Z' + y][0] + suff
	except:
		pass
	for i in range(len(y), -1, -1):
		suff = y[:i]
		try:
			v = dict[x + suff]
			return v[0]
		except:
			pass
		if suff == 'ZV':
			for suff in ['ZVV', 'ZVA']:
				try:
					(res,) = dict[x + suff]
					return res
				except:
					pass
	return None

def phoneme_process(dictionary, for_dict, text, shell=False):
	word_split, _ = kiwi.analyze(text)[0]
	dtext = hangeul.denormalize(text)
	word_split = [(a, b) for a, b, c, d in word_split]
	alignment_result = algorithm.get_difference(dtext, word_split, text) # alignment_result = alignment(dtext, word_split)
	alignment_result = [(a, b, c, d, e, hangeul.normalize(dtext[c:d]) == hangeul.normalize(hangeul.denormalize(a)[:-e] if e else a)) for a, b, c, d, e in alignment_result]
	if shell:
		print(alignment_result)
	r = []
	phoneme_info = [(x, None, None) for x in dtext]
	for i, (a, b, c, d, erase_count, e) in enumerate(alignment_result):
		if e:
			for j in range(c, d):
				f, s, word_index = phoneme_info[j]
				if s is None:
					s = b
					word_index = i
				else:
					s = None
					word_index = None
				phoneme_info[j] = (f, s, word_index)
		next = None
		if i + 1 < len(alignment_result):
			_a, _b, _c, _d, _, _e = alignment_result[i + 1]
			if _e:
				next = _b
		r.append((a, b, c, d, e, erase_count, next))
	alignment_result = r
	r = []

	override_next = False
	ceok = False
	for i, (a, b, c, d, e, ec, nf) in enumerate(alignment_result):
		if b[0] == 'V': # 용언, "다" 붙여서 검색 후 '다' 떼고 받침 원래대로
			dict_fail = True
			if nf is not None and nf[0] == 'E': # 사전에 활용 있는지 검사
				for nd in range(d, d+5):
					zword = hangeul.normalize(dtext[c:nd])
					val = find_dict(dictionary, zword, 'Z' + b)
					if val is not None:
						r.append((i, (zword, 'Z' + b, c, nd, hangeul.denormalize(val), alignment_result[i+1][-1])))
						override_next = True
						dict_fail = False
						break
			if dict_fail:
				val = find_dict(dictionary, a + '다', b)
				ending_hieut = ord(hangeul.denormalize(a[-1])[-1]) == 0x11c2
				if ec:
					a = hangeul.normalize(hangeul.denormalize(a)[:-ec])
				if val is not None:
					val = val[:-1]
					val = hangeul.denormalize(val)
					if ending_hieut: # 사전 용언의 어간 받침 'ㅎ'을 살림
						val += chr(0x11c2)
					if ec > 0:
						val = val[:-ec]
					if e: # reliable한 경우는
						nf = (alignment_result[i+1][1] if i + 1 < len(alignment_result) else None) if nf is None else nf
						# 뒤 단어가 형식 형태소이고 모음이며 앞 단어가 종성인 경우만, 'ㄶ', 'ㅀ'은 예외적으로 거센소리되기를, 'ㄵ', 'ㄻ', 'ㄼ', 'ㄾ'은 된소리되기를 위해
						if ord(hangeul.denormalize(a[-1])[-1]) in {0x11ad, 0x11b6, 0x11ac, 0x11b1, 0x11b2, 0x11b4} or (
							nf is not None and (nf[0] in 'JE' or nf == 'VCP') and 0x1161 <= ord(hangeul.denormalize(alignment_result[i+1][0][0])[0]) <= 0x1175
							and 0x11a8 <= ord(hangeul.denormalize(a[-1])[-1]) <= 0x11c2
						):
							val = val[:-1] + hangeul.denormalize(a[-1])[-1] # 연음화를 위해 원래 받침을 살림
					else: # 그렇지 못한 경우는 끝자리에서만 축약/탈락 등의 음운 현상이 일어날 수 있으므로, 마지막 한 음운을 제한 앞부분만 교체
						val = val[:-1]
						d -= 1
				r.append((i, (a, b, c, d, val, nf)))
		elif e and not override_next:
			# "-적"은 관형격 수식의 접미사. 사전에 형태가 대부분 있으므로 일단 사전부터 찾아봄
			if nf is not None and nf[:2] == 'XS' and alignment_result[i+1][0] == '적':
				word = a + '적'
				search_result = find_dict(dictionary, word, 'MM')
				if search_result is not None:
					r.append((i, (word, 'MM', c, alignment_result[i+1][3], hangeul.denormalize(search_result), alignment_result[i+1][-1])))
					ceok = True
			elif not ceok:
				val = find_dict(for_dict, a, b)
				# "사전을 찾기 전에 외래어 사전 먼저 찾도록 처리"
				if val is not None:
					val = hangeul.denormalize(val)
					if ec:
						a = hangeul.normalize(hangeul.denormalize(a)[:-ec])
						val = val[:-ec]
					r.append((i, (a, b, c, d, val, nf)))
					continue

				val = find_dict(dictionary, a, b)
				if val is not None:
					val = hangeul.denormalize(val)
				if ec:
					a = hangeul.normalize(hangeul.denormalize(a)[:-ec])
					if val is not None:
						val = val[:-ec]
				r.append((i, (a, b, c, d, val, nf)))
			else:
				ceok = False
		elif override_next:
			override_next = False
		else:
			if ec:
				a = hangeul.normalize(hangeul.denormalize(a)[:-ec])
			r.append((i, (a, b, c, d, None, nf)))

	for idx, (i, (a, b, c, d, e, nf)) in reversed(list(enumerate(r))):
		if e is not None:
			res = [(x, b, i) for x in e]
			# 음절 끝소리 규칙; 용언은 따로 처리하였음, 모음인 경우 ㅢ가 ㅣ가 되는 경우 등 처리해서는 안 됨
			if b[0] != 'V' and nf is not None and (nf[0] == 'J' or nf == 'VCP' or nf[0] == 'E' or (nf[0] == 'X' and nf != 'XR')):
				if ord(res[-1][0]) >= 0x11a8:
					res[-1] = (hangeul.denormalize(a[-1])[-1], b, i)
			phoneme_info = phoneme_info[:c] + res + phoneme_info[d:]
		# 제16항: 디귿, 지읒, 치읓, 키읔, 티읕, 피읖, 히읗
		# 일어날 다른 음운 현상이 없으므로, 첨가 등 다른 조건에 무관하게 무자비하게 적용
		if b is not None and b[:2] == 'NN' and a in {'디귿', '지읒', '치읓', '키읔', '티읕', '피읖', '히읗'}:
			phoneme_info = phoneme_info[:d-1] + [(chr(0x11a8 if a == '키읔' else 0x11b8 if a == '피읖' else 0x11ba), 'PC', i)] + phoneme_info[d:]
		# ㄴ 첨가, 사이시옷 들어가는 경우는 단어장에 대부분 있으며, 없는 경우 제30항 허용에 따라 올바름
		# 품사 조건, b[0] in 'NX' and (nf is not None and nf[0] in 'NX') 들어가야 하나?
		# 의존 명사의 경우를 추가하지 말 것: 고시를 볼 양이면[고시를 볼량이면] 등의 예시가 있음
		elif nf is not None and (nf[0] not in 'JE' and nf != 'VCP' and nf[:2] != 'XS'):
			# 음운 조건: 앞 단어의 끝이 자음이고 뒤 단어가 모음으로 시작하며 'ㅣ', 'ㅑ', 'ㅕ', 'ㅛ', 'ㅠ'인 경우
			if ord(hangeul.denormalize(a[-1])[-1]) >= 0x11a8 and ord(hangeul.denormalize(r[idx + 1][1][0][0])[0]) in [0x1175, 0x1163, 0x1167, 0x116d, 0x1172]:
				# 숫자 일, 이가 뒤에서 오는 경우 ㄴ 첨가 현상이 일어나지 않음 (육이오[유기오], 삼일절[사밀쩔], 십이[시비] 등)
				# 날짜를 세는 의존 명사 "-일"에는 ㄴ 첨가 현상이 일어나지 않음 (삼월 십팔일 [사뭘 십파릴] 등)
				if r[idx+1][1][0] + r[idx+1][1][1] not in ['일NR', '이NR', '일NNB']:
					# 형태소 분석기의 오류인 "멋있"/"맛있" 은 "머싣"/"맛있" 으로 읽히게 예외처리함.
					if r[idx][1][0] + r[idx][1][1] in ['멋NNG', '맛NNG']:
						# 받침에 디귿 대신 시옷으로 치환
						phoneme_info = phoneme_info[:d-1] + [(chr(0x1109), 'PA', i)] + phoneme_info[d:]
					elif ord(phoneme_info[d][0]) in [0x1175, 0x1163, 0x1167, 0x116d, 0x1172]:
						# ㄴ 첨가 현상
						phoneme_info = phoneme_info[:d] + [(chr(0x1102), 'PA', i)] + phoneme_info[d:]

	data = []
	for x, f, i in phoneme_info:
		# 마지막 처리를 할 때에는 활용형을 고려하지 않음
		uf = f
		if uf is not None and uf.startswith('Z'):
			uf = uf[1:]
		if data:
			l, lf, li = data[-1]
			ulf = lf
			if ulf is not None and ulf.startswith('Z'):
				ulf = ulf[1:]
			same = i == li
			# 활용형인 경우 어미를 포함하고 있으므로 어미가 들어오는 경우는 같도록 처리
			if lf is not None and lf[:2] == 'ZV' and f is not None and f[:1] == 'E':
				same = True
			result = hangeul.merge(l, ulf, x, uf, same)
			if result is not None:
				a, b = result
				data.pop()
				if a:
					data.append((chr(0x11a7 + a), f, i))
				if b != 11:
					data.append((chr(0x1100 + b), f, i))
				if 0x1161 <= ord(x) <= 0x1175:
					data.append((x, f, i))
			else:
				data.append((x, f, i))
		else:
			data.append((x, f, i))
	phoneme_result = hangeul.normalize("".join((lambda y: x[0] if y is None else y)(hangeul.jongseong(x[0])) for x in data))
	return algorithm.make_clean(text, phoneme_result)

def load_dict(dict_path):
	with open(dict_path, 'r', encoding='utf-8') as f:
		l = f.readlines()
	d = {}
	for x in l:
		u = x.strip().split()
		d[u[0]] = u[1:]
	return d

if __name__ == '__main__':
	print('Loading Kiwi... ', end="")
	sys.stdout.flush()
kiwi = Kiwi(options=Option.LOAD_DEFAULT_DICTIONARY)
kiwi.load_user_dictionary('./custom-dict.txt')
kiwi.prepare()
if __name__ == '__main__':
	print('OK.')

def clean(x, y):
	if len(x) != len(y):
		return False
	for c,d in zip(x, y):
		if c != d:
			if not (44032 <= ord(c) < 55204 and 44032 <= ord(d) < 55204):
				return False
	return True

def main():
	while True:
		try:
			d = input('>>> ')
		except EOFError:
			print()
			break
		if d.startswith('d '):
			w = d[2:]
			z = None
			if '/' in w:
				w, z = w.split('/')
			result = []
			for key, val in dictionary.items():
				if key.startswith(w) and (z is None or z in key):
					result.append((key, key + ': ' + ', '.join('[%s]' % x for x in val)))
			result.sort(key=lambda x:x[0])
			print('\n'.join(x[1] for x in result))
		else:
			try:
				s = phoneme_process(dictionary, for_dict, d, shell=True)
				print('CLEAN' if clean(d, s) else 'DIRTY', s)
			except:
				traceback.print_exc()

def process(file, for_tts = False):
	with open(file, 'r', encoding='utf-8') as f:
		data = f.readlines()
	if for_tts:
		wavpath = [x.split('|')[0].strip() for x in data]
		spk_id = [x.split('|')[2].strip() for x in data]
		data = [x.split('|')[1].strip() for x in data]
	else:
		data = [x.split('\t')[0].strip() for x in data]

	result = []
	failed = 0
	for i, x in enumerate(data):
		try:
			prep = phoneme_process(dictionary, for_dict, x)
			if not clean(prep, x):
				raise ValueError(prep)
		except:
			prep = 'ERROR!'
			failed += 1
		if for_tts:
			result.append('%s|%s|%s\n' % (wavpath[i],prep,spk_id[i]))
		else:
			result.append('%s\t%s\n' % (x, prep))
		if i % 200 == 0:
			sys.stderr.write('\r%6.2f%%' % (i / len(data) * 100))
			sys.stderr.flush()
	outpath = file.replace(".txt","_g2p.txt")
	with open(outpath, 'w', encoding='utf-8') as f:
		f.write("".join(result))
	print('\r100.00%')
	print('%d out of %d failed' % (failed, len(data)))

if __name__ == '__main__':
	print('Loading Dictionary... ', end="")
	sys.stdout.flush()
	dictionary = load_dict('word_dict.txt')
	for_dict = load_dict('foreign_dict.txt')
	print('OK.')

	if len(sys.argv) == 1:
		main()
	else:
		if len(sys.argv) == 3:
			if sys.argv[2] == '--for_tts':
				process(sys.argv[1], for_tts=True)
			else:
				print("invalid option")
		else:
			process(sys.argv[1])

