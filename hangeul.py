def is_hangeul(x):
	return 44032 <= ord(x) < 55204 or 0x3130 <= ord(x) < 0x3190

def complete_hangeul(x):
	return all(44032 <= ord(y) < 55204 for y in x)

def split(x):
	if 44032 <= ord(x) < 55204:
		o = ord(x) - 44032
		cho, jung, jong = o // 588, (o // 28) % 21, o % 28
		return (cho, jung, jong)
	return None

def construct(cho, jung, jong):
	return chr(44032 + cho * 588 + jung * 28 + jong)

def vowel_start(x):
	try:
		cho, jung, jong = split(x[0])
		if cho == 11:
			return jung
	except:
		return None
	return None

jong2cho_dict = [
	-1, 0, 1, None, 2, None, 2, 3, 5,
	None, None, None, None, None, None, 5, # ㄹ계 합용 병서
	6, 7, None, 9, 10, 11, 12, 14, 15, 16, 17, -1
]
def jong2cho(jong):
	try:
		return jong2cho_dict[jong]
	except:
		return None

strong_sound_dict = [
	15, 15, None, 16, 16, None, None,
	17, 17, None, None, None, 14, 14,
	14, 15, 16, 17, None
]
def strong_sound(cho):
	try:
		return strong_sound_dict[cho]
	except:
		return None

def denormalize(text):
	l = []
	for i,x in enumerate(text):
		try:
			cho, jung, jong = split(x)
			cho = "" if cho == 11 else chr(0x1100 + cho)
			jung = chr(0x1161 + jung)
			jong = "" if jong == 0 else chr(0x11a7 + jong)
			l.append(cho + jung + jong)
		except:
			l.append(x)
	return "".join(l)

def normalize(dtext, single_denorm = False):
	length = len(dtext)
	if length >= 3:
		r = [dtext[0], dtext[1]]
		for i in range(2, length):
			a, b, c = [ord(dtext[i + d]) for d in range(-2, 1)]
			if 0x1100 <= a <= 0x1112 and 0x1161 <= b <= 0x1175 and 0x11a8 <= c <= 0x11c2:
				r.pop()
				r.pop()
				r.append(chr(44032 + 588 * (a - 0x1100) + 28 * (b - 0x1161) + (c - 0x11a7)))
			else:
				r.append(dtext[i])
		text = "".join(r)
	else:
		text = dtext
	length = len(text)
	if length >= 2:
		r = [text[0]]
		for i in range(1, length):
			a, b = [ord(text[i + d]) for d in range(-1, 1)]
			if 0x1100 <= a <= 0x1112 and 0x1161 <= b <= 0x1175:
				r.pop()
				r.append(chr(44032 + 588 * (a - 0x1100) + 28 * (b - 0x1161)))
			elif 0x1161 <= a <= 0x1175 and 0x11a8 <= b <= 0x11c2:
				r.pop()
				r.append(chr(44032 + 588 * 11 + 28 * (a - 0x1161) + (b - 0x11a7)))
			else:
				r.append(text[i])
		text = "".join(r)
	r = []
	if single_denorm:
		r = text
	else:
		for x in text:
			cho = list('ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ')
			jung = list('아애야얘어에여예오와왜외요우워웨위유으의이')
			jong = list('ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ')
			o = ord(x)
			if 0x1100 <= o <= 0x1112:
				r.append(cho[o - 0x1100])
			elif 0x1161 <= o <= 0x1175:
				r.append(jung[o - 0x1161])
			elif 0x11a8 <= o <= 0x11c2:
				r.append(jong[o - 0x11a8])
			else:
				r.append(x)
	return "".join(r)

merge_table = None

def merge(c, cf, d, df, same):
	global merge_table
	if merge_table is None:
		merge_table = {}
		with open('keussori.txt', encoding='utf-8') as f:
			for x in f.readlines():
				a, b, v = x.strip().split()
				merge_table[a + b] = v.split('/')
	if 0x1200 > ord(c) >= 0x11a8 and 0x1100 <= ord(d) <= 0x1175:
		oc = c
		c = normalize(c)
		if df is not None and ((df[0] in 'JX' and df != 'XR') or df == 'VCP') and c in 'ㄷㅌㄾ' and ord(d) == 0x1175: # 구개음화
			return ({'ㄷ': (0, 12), 'ㅌ': (0, 14), 'ㄾ': (8, 14)})[c]
		if ord(d) <= 0x1112:
			d = normalize(d)
		else:
			d = 'ㅏ'
		if (cf is not None and cf[0] == 'E') and c == 'ㄹ' and d in 'ㄱㄷㅂㅅㅈ': # 관형형 어미 'ㄹ' 뒤에 오는 예삿소리
			return (8, ({'ㄱ': 1, 'ㄷ': 4, 'ㅂ': 8, 'ㅅ': 10, 'ㅈ': 13})[d])
		try:
			if df is not None and (df[0] not in 'JEX' and df != 'VCP') and not same:
				c = normalize(jongseong(oc))
			# 명사인데 ㅎ으로 끝나는 경우 ㅅ으로 발음하는 경향, 제16항에 준함 (아햏햏이 무엇인가? -> 아해태시 무어신가?)
			if not same and cf is not None and cf[:2] == 'NN' and ord(oc) == 0x11c2:
				c = 'ㅅ'
			x,y = merge_table[c + d]
		except KeyError:
			return None
		cho = list('ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ')
		jong = list('?ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ')
		if x:
			x = jong.index(x)
		else:
			x = 0
		if y:
			y = cho.index(y)
		else:
			y = 11
		if not same and cf is not None and cf[0] == 'V' and df is not None and df[0] == 'E' and c in 'ㄴㄵㄻㄼㄾㅁ' and d in 'ㄱㄷㅅㅈ': # 24항, 25항
			return (x, y + (y in [0, 3, 7, 9, 12]))
		return (x, y)
	return None

def jongseong(u):
	u = ord(u)
	jong = list('ㄱㄱㄱㄴㄴㄴㄷㄹㄱㅁㄹㄹㄹㅂㄹㅁㅂㅂㄷㄷㅇㄷㄷㄱㄷㅂㄷ')
	if 0x11a8 <= u <= 0x11c2:
		return chr(({'ㄱ': 0x11a8, 'ㄴ': 0x11ab, 'ㄷ': 0x11ae, 'ㄹ': 0x11af, 'ㅁ': 0x11b7, 'ㅂ': 0x11b8, 'ㅇ': 0x11bc})[jong[u - 0x11a8]])
	return None
