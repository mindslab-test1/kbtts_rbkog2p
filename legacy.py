from algorithm import TwoDimArray
import hangeul

def alignment(denormalized, result):
	pos = 0
	res = []
	abbrevated = None
	for word, form in result:
		denorm_word = hangeul.denormalize(word)
		if 0x1100 <= ord(denorm_word[0]) <= 0x1200:
			failback = pos
			while pos < len(denormalized) and denormalized[pos] == ' ':
				pos += 1
			if pos >= len(denormalized):
				break
			last_pos = pos
			for i, x in enumerate(denorm_word):
				if i == 0 and abbrevated: # abbrevation occurred; last and first ones must be merged
					abbrevated = False
					if not (0x1161 <= ord(x) <= 0x1175) or 0x1161 <= ord(denormalized[failback]) <= 0x1175:
						last_pos = failback
						pos = failback + 1
						continue
					else:
						last_pos += 1
						pos += 1
				if x != denormalized[pos]: # hell yeah
					if form is not None and (form[0] == 'V' or form == 'XSV' or form == 'XSA') and word == '하': # 완전히 줄어든 '하'
						abbrevated = False
						break
					# 같은 모음이 탈락되는 경우 및 이형태
					# 길이 1의 단어들(오(다), -어 등)의 경우 일단 조사/어미인 경우만 앞과 합쳐지도록 처리
					elif (len(denorm_word) >= 2 and i == 0) or (len(denorm_word) <= 1 and form[0] in 'JE'):
						abbrevated = False
						if form != 'VCP' and (0x1161 <= ord(x) <= 0x1175) == (0x1161 <= ord(denormalized[pos - 1]) <= 0x1175):
							last_pos = failback - 1
							pos = failback
							continue
					elif i + 2 == len(denorm_word): # 종성/초성 음운이 일치하지 않으면, 일단 "그렇 + 어 -> 그래" 및 "것 + 이 -> 게"만 인정
						if form[0] == 'V' and word == '그렇' and ord(denormalized[pos]) == 0x1162:
							abbrevated = True
							res.append(('그래', form, last_pos, pos + 1)) # "그래" 사이에서는 일어날 음운 현상이 없으므로
							break
						elif form == 'NNB' and word == '것' and ord(denormalized[pos]) == 0x1166:
							abbrevated = True
							res.append(('거', form, last_pos, pos + 1))
							break
						elif hangeul.normalize(x) != hangeul.normalize(denormalized[pos]): # THIS is a heavy task (about 15x slower), do not try to optimize it with the "if x != denormalized[pos]" part!
							raise ValueError(denorm_word)
					elif i + 1 == len(denorm_word): # 축약?
						if form[0] == 'V' and ord(x) == 0x11ae and ord(denormalized[pos]) == 0x11af: # 묻다 -> 물었다 (음운 변화)
							pass
						else:
							abbrevated = True
							res.append((word, form, last_pos, pos + 1))
							break
					else: # catastrophic failure
						raise ValueError(denorm_word)
				abbrevated = False
				pos += 1
			else: # looks good!
				res.append((word, form, last_pos, pos))
				abbrevation = False
		else:
			while pos < len(denormalized) and denormalized[pos] == ' ':
				pos += 1
			if pos >= len(denormalized):
				break
			last_pos = pos
			for i, x in enumerate(denorm_word):
				if denormalized[pos] != x:
					raise ValueError(denorm_word)
				pos += 1
			res.append((word, form, last_pos, pos))
	return res

def legacy_edit_distance(original, modified):
	D = TwoDimArray()
	for i,c in enumerate(original):
		for j,d in enumerate(modified):
			D[i+1,j+1] = min(D[i,j+1] + 1, D[i+1,j] + 1, D[i,j] + +(c!=d))
	return D
