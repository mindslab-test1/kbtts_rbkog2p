import time
import random
import argparse

from client import G2PClient


def main(client, text_path):
    def load_metadata(path, split="|"):
        with open(path, 'r', encoding='utf-8') as f:
            metadata = [line.strip().split(split) for line in f]
        return metadata

    metadata = load_metadata(text_path)
    print('Loaded %d lines of text from %s' % (len(metadata), text_path))
    print('Sample text: %s' % random.choice(metadata)[1])

    histogram_buckets = [0, 10, 25, 50, 100, 150]
    bucket_counts = [0 for _ in range(len(histogram_buckets))]
    bucket_sums = [0.0 for _ in range(len(histogram_buckets))]
    for line_idx, line in enumerate(metadata):
        text_length = len(line[1])

        # measure time
        start = time.time()
        x = client.transliterate([line[1]]).sentences
        end = time.time()
        elapsed = end - start

        # construct histogram
        for idx in range(len(histogram_buckets)-1):
            if text_length < histogram_buckets[idx+1]:
                bucket_counts[idx] += 1
                bucket_sums[idx] += elapsed
                break
        else:
            bucket_counts[-1] += 1
            bucket_sums[-1] += elapsed

        print('\r%6.2f%%' % (100.0 * line_idx / len(metadata)), end="")
    print('\r100.00%%')

    for idx in range(len(histogram_buckets)-1):
        if bucket_counts[idx] == 0:
            continue
        print('%4d - %4d: %6.2fms (from %6d samples)' % \
            (histogram_buckets[idx], histogram_buckets[idx+1],
                1000.0*bucket_sums[idx]/bucket_counts[idx], bucket_counts[idx]))

    if bucket_counts[-1] > 0:
        print('%4d <       : %6.2fms (from %6d samples)' % \
            (histogram_buckets[-1],
                1000.0*bucket_sums[-1]/bucket_counts[-1], bucket_counts[-1]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='rbkog2p speed tester')
    parser.add_argument('-t', '--text_path', type=str, required=True,
                        help='TTS metadata path')
    args = parser.parse_args()

    client = G2PClient('127.0.0.1:19002')

    main(client, args.text_path)
